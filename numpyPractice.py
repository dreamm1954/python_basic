import numpy as np
#Generate Sequence 
l1 = [i for i in range(20) if i%2==0]
print("l1 = %s"%(l1))
#Generate numpy array
ary = np.array(l1)
print("ary = %s"%(ary))
#Square each element of array
squareAry = np.square(ary)
print("squareAry = %s"%(squareAry))
#calculate median of array
print("median of ary = %s"%(np.median(ary)))
#calculate mean of array
print("mean of ary = %s"%(np.mean(ary)))
#calculate variance of array
print("variance of ary = %s"%(np.var(ary)))
#calculate standard deviation of array
print("standard deviation of ary = %s"%(np.std(ary)))
#calculate sqrt of array
print("square root of ary = %s"%(np.sqrt(ary)))

#Generate array of random values over [0,1)
randAry = np.random.rand(20)
print("randAry = %s"%(randAry))
#Generate array of random integers over [0,10)
randIntAry = np.random.randint(10,size=10)
print("randIntAry = %s"%(randIntAry))
#Sort of randIntAry
print("sort of randIntAry = %s" % (np.sort(randIntAry)))
print("reverse sort of randIntAry = %s" % ((np.sort(randIntAry))[::-1]))

#generate array from file
npd = np.genfromtxt('data.csv', delimiter=',')
print("data of npd = %s"%(npd))
